// Copyright Epic Games, Inc. All Rights Reserved.

#include "ue_snake.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ue_snake, "ue_snake" );
